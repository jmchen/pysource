#!/usr/bin/python

import xmlrpclib,sys

url='http://127.0.0.1:8000'
s=xmlrpclib.ServerProxy(url)
methods=s.system.listMethods()
while 1:
    print '\n\nAvailable Methods:'
    for i in range(len(methods)):
        print '%2d:%s'%(i+1,methods[i])
    selection=raw_input("seldct one (q to quit):")
    if selection=='q':
        break
    item=int(selection)-1
    print '\n********'
    print "Details for %s \n"%methods[item]
for sig in s.system.methodHelp(methods[item]):
    print 'args:%s;returns :%s'%(",".join(sig[1:]),sig[0])
print "help:",s.system.methodHelp(methods[item])
