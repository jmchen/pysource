#include<stdio.h>

struct A{
	int num;
};

struct B{
	int num;
	char type;
	int age;
};

int main()
{
	struct A a;
	a.num=1;

	char* tmp1=(char *)(&(a.num));
	tmp1=tmp1+4;
	*tmp1='a';
	int *tmp2=(int *)(&(a.num));
	tmp2=tmp2+2;
	*tmp2=0x1111111;

	struct B *b=(struct B *)(&a);

	printf(" b->num=%d  b->type=%c  b->age=%d \n",b->num,b->type,b->age);
    printf("%d,%d\n",sizeof(int),sizeof(char));
}