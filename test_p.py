import sys
import time
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from functools import partial

class Button(QDialog):
    def __init__(self,parent=None):
        super(Button,self).__init__(parent)
        self.button1=QPushButton("one")
        self.button2=QPushButton("two")
        self.button3=QPushButton("three")
        self.label=QLabel("Sleeping...")
        layout=QGridLayout()
        layout.addWidget(self.button1, 0, 0)
        layout.addWidget(self.button2, 0, 1)
        layout.addWidget(self.button3, 0, 2)
        layout.addWidget(self.label, 1, 0)
        self.setLayout(layout)
       # self.button1Callback=partial(self.clickButon, "One")
        #self.button2Callback=partial(self.clickButon, "Two")
        #self.button3Callback=partial(self.clickButon, "Three")
        self.button1Callback=lambda which="One":self.clickButon(which)
        
        self.connect(self.button1, SIGNAL("clicked()"), self.button1Callback)
        self.connect(self.button2, SIGNAL("clicked()"), self.clicked)
       # self.connect(self.button3, SIGNAL("clicked()"),self.button3Callback )
        
    def clickButon(self, which):
        self.label.setText("You just click %s"%which)
    def one(self):
        self.label.setText("one")
        
    def  clicked(self):
        button=self.sender()
        if button is None or not isinstance(button, QPushButton):
            return 
        self.label.setText("Haha,use sender(),you just click %s"%button.text())
        
    def setPenProperties(self):
        dialog=PenPropertiesDlg(self)
        dialog.widthSpinBox.setValue(self.width)
        dialog.beveledCheckBox.setChecked(self.beveled)
        dialog.styleComboBox.setCurrentIndex(
                                             dialog.styleComboBox.findText(self.style))
        if dialog.exec_():
            self.width=dialog.widthSpinBox.value()
            self.beveled=dialog.beveledCheckBox.isChecked()
            self.style=unicode(dialog.styleComboBox.currentText())
            self.updateData()
app=QApplication(sys.argv)
button=Button()
button.show()
app.exec_()

        
