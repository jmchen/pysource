from random import randint
from time import sleep,ctime
from Queue import Queue
import threading


class Mythread(threading.Thread):
    def __init__(self,func,name,args):
        threading.Thread.__init__(self)
        self.name=name
        self.args=args
        self.func=func
    def run(self):
        print 'start func:',self.name,'at:' ,ctime()
        apply(self.func,self.args)

def writeQ(queue):
    print '^-^produceing object ...'
    queue.put('xxxx',1)
    print '^-^size now:',queue.qsize()

def readQ(queue):
    val=queue.get(1)
    print '^-^consumed object from Q....' 
    print '^-^size now:',queue.qsize()

def writer(queue,loops):
    for i in range(loops):
        writeQ(queue)
        sleep(randint(1,3))

def reader(queue,loops):
    for i in range(loops):
        readQ(queue)
        sleep(randint(2,5))

func=[writer,reader]
nfuncs=range(len(func))

def main():
    q=Queue(10)
    threads=[]
    nloops=randint(2,5)
    for i in nfuncs:
        t=Mythread(func[i],func[i].__name__,(q,nloops))
        threads.append(t)
    for i in nfuncs:
        threads[i].start()
    for i in nfuncs:
        threads[i].join()
    print 'all done'
if __name__=='__main__':
    main()


