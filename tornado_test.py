import tornado.ioloop
import tornado.web

class BaseAuth(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user")

class MainHandler(BaseAuth):
    def get(self):
        if not self.current_user:
            self.redirect("/login")
            return
        name=tornado.escape.xhtml_escape(self.current_user)
        self.write("hello:%s"%name)



class LoginHandler(BaseAuth):
    def get(self):
        self.write('<html><body><form action="/login" method="post">'
                                      'Name: <input type="text" name="name">'
                                      '<input type="submit" value="Sign in">'
                                      '</form></body></html>')
    def post(self):
        self.set_secure_cookie("user",self.get_argument("name"))
        self.redirect("/")

            

class StoryHandler(tornado.web.RequestHandler):
    def get(self,story_id):
        self.write("the numberis "+story_id)



applecation=tornado.web.Application([
        (r"/",MainHandler),
        (r"/story/([0-9]+)",StoryHandler),
        (r"/login",LoginHandler),

        ],cookie_secret="8487167117")


if __name__=="__main__":
        applecation.listen(8888)
        tornado.ioloop.IOLoop.instance().start()


        

        
























