# -*- coding: utf-8 -*-
def wrapfun(func):
    def wrappedFunc():
        print '%s 调用'%(func.__name__)
        return func()
    return wrappedFunc
@wrapfun
def print_a():
    pass
if __name__=="__main__":
    print_a()
