import logging 
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import uuid

from tornado.options import define,options

define("port",default=8888,help="run on  the given port",type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers=[
            (r"/",MainHandler),
            (r"/chatsocket",ChatSockethandler),
        ]
        settings=dict(cookie_secret="ksdjf;asfjlfjlasdj",
                      template_path-os.path.join(os.path.dirname(__fie__),"tmeplates"),
                      static_path=os.path.join(os.path.dirname(__fie__),"static"),
                      xsrf_cookies=True,
                      autoescape=None,

                     )
        tornado.web.Application.__init__(self,handlers,**settings)

class MainHandle(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html",messages=ChatSocketHandler.cache)

class ChatSockethandler(tornado.websocket.WebSocketHandler):
    waiters=set()
    cache=[]
    cache_size=200

    def allow_draft(self):
        return True
    def open(self):
        ChatSocketHandler.waiters.add(self)

    def on_close(self):
        ChatSocketHandler.waiters.remove(self)
    @classmethod
    def update_cache(cls,chat):
        cls.cache.append(chat)
        if len(cls.cache)>cls.cache_size:
            cls.cache=cls.cache[-cls.cache_size:]

    @classmethod
    def send_update(cls,chat):
        logging.info("sending message to %d waiters",len(cls.waiters))
        for waiters in cls.waiters:
            try:
                waiters.write_message(chat)
            except:
                logging.error("error sending message",exc_info=True)

    def on_message(self,messages):
        logging.info("got meaaage %r ",messages)
        parsed=tornado.escape.json_decode(message)
        chat={
            "id":str(uuid.uuid4()),
            "body":parsed["body"],
        }
        chat["html"]=self.render_string("message.html",message=chat)
        ChatSocketHandler.update_cache(chat)
        ChatSocketHandler.send_update(chat)

def main():
    tornado.options.parse_command_line()
    app=Application()
    app.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

if __name__=="__main__":
        main()
        


