import sys
import os
sys.path.insert(0, os.getcwd())

CELERY_IMPORTS = ("tasks",)

BROKER_URL = 'redis://localhost:6379/0'

CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
