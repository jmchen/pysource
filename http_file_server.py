import sys
import string
import SimpleHTTPServer
import SocketServer

if len(sys.argv)!=3:
	sys.exit()

addr=sys.argv[1]
port=string.atoi(sys.argv[2])

handle=SimpleHTTPServer.SimpleHTTPRequestHandler
httpd=SocketServer.TCPServer((addr,port),handle)
print "http server is : ",addr,port
httpd.serve_forever()
