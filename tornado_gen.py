import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.httpclient
import tornado.gen

import urllib
import json
import datetime
import time

from tornado.options import define,options
define("port",default=8000,help="run on the server",type=int)

class IndexHandle(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.engine
    def get(self):
        query=self.get_argument('q')
        client=tornado.httpclient.AsyncHTTPClient()
        response=yield tornado.gen.Task(client.fetch,
                                        "http://search.twitter.com/search.json?"+\
                                        urllib.urlencode({ "q":query,"result_type":"recent","rpp":100 })
                                       )
        body=json.loads(response.body)
        result_count=len(body['results'])
        now=datetime.datetime.utcnow()
        raw_oldest_tweet_at=body['results'][-1]['created_at']
        oldest_tweet_at=datetime.datetime.strptime(raw_oldest_tweet_at,"%a,%d %b %Y:%M:%S +0000")
        second_diff=time.mktime(now.timetuple())-\
                time.mktime(oldest_tweet_at.timetuple())
        tweets_per_second=float(result_count)/second_diff
        self.write("""
        <div sytle="text-align:center">
            <div style="font-size:72px">%s</div>
            <div sytle="font-size:144px">%.02f</div>
            <div  style="font-size:24px">tweets per second</div>
        </div>"""%(query,tweets_per_second))
        self.finish()

if __name__=="__main__":
    tornado.options.parse_command_line()
    app=tornado.web.Application(handlers=[(r"/",IndexHandle)])
    http_server=tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()






