import os
import platform
import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
#import helpform 
import newimagedlg
import qrc_resources

__version__="1.0.0"

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)
        self.image=QImage()
        self.dirty=False
        self.filename=None
        self.mirroredvertically=False
        self.mirroredhorizontally=False

        self.imageLabel=QLabel()
        self.imageLabel.setMinimumSize(200,200)
        self.imageLabel.setAlignment(Qt.AlignCenter)
        self.imageLabel.setContextMenuPolicy(Qt.ActionContextMenu)
        self.setCentralWidget(self.imageLabel)

        logDockWidget=QDockWidget("Log",self)
        logDockWidget.setObjectName("LogDockWidget")
        logDockWidget.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightdockWidgetArea)
        self.listWidget=QListWidget()
        logDockWidget.setWidget(self.listWidget)
        self.addDockWidget(Qt.RightdockWidgetArea,logDockWidget)
        self.printer=None
        self.sizeLabel=QLabel()
        self.sizeLabel.setFrameStyle(QFrame.StyledPanel | QFrame.Sunken)
        status=self.statusBar()
        status.setSizeGripEnable(False)
        status.addPermanentWidget(self.sizeLabel)
        status.showMessage("Ready",5000)
        
        #fileNewAction=QAction(QIcon("images/filenew.png"), "&New", self)
        #fileNewAction.setShortcut(QKeySequence.New)
        #helpText="Create a new image"
        #fileNewAction.setToolTip(helpText)
        #fileNewAction.setStatusTip(helpText)
        #self.connect(fileNewAction, SIGNAL("triggered()"), self.fileNew)
        
        #fileMenu.addAction(fileNewAction)
        #fileToolbar.addAction(fileNewAction)
        fileNewAction=self.createAction("&New", self.fileNew, QKeySequence.New, "filenew", "Create an image file")
        fileQuitAction=self.createAction("&Quit", self.close, "Ctrl+Q", "filequit", "Close the application")
        editZoomAction=self.createAction("&Zoom", self.editZoom, "Alt+Z", "editzoom", "Zoom the image")
        
        mirrorGroup=QActionGroup(self)
        editUnMirrorAction=self.createAction("&Unmirror", self.editUnMirror, "Ctrl+U", "editunmirror", "Unmirror the image", True, "toggled(bool)")
        mirrorGroup.addAction(editUnMirrorAction)
        editUnMirrorAction.setChecked(True)
        
        editMenu=self.menuBar().addMenu("&Edit")
        self.addActions(editMenu, (editInvertAction, editSwapRedAndBlueAction, editZoomAction))
        
        self.fileMenu=self.menuBar().addMenu("&File")
        self.fileMenuActiions=(fileNewAction, fileOpenAction, fileSaveAction, fileSaveAsAction, None, filePrintAction, fileQuitAction)
        self.connect(self.fileMenu, SIGNAL("aboutToShow()"), self.updateFileMenu)
        
        fileToolbar=self.addToolBar("File")
        fileToolbar.setObjectName("FileToolBar")
        self.addActions(fileToolbar, (fileNewAction, fileOpenAction, fileSaveAsAction))
        
        editToolbar=self.addToolBar("Edit")
        editToolbar.setObjectName("EditToolBar")
        self.addActions(editToolbar, (None))
        
        self.zoomSpinBox=QSpinbox()
        self.zoomSpinBox.setRange(1, 400)
        self.zoomSpinBox.setSuffix("%")
        self.zoomSpinBox.setValue(100)
        self.zoomSpinBox.setToolTip("Zoom the images")
        self.zoomSpinBox.setStatusTip(self.zoomSpinBox.toolTip())
        self.zoomSpinBox.setFocusPolicy(Qt.NoFocus)
        self.connect(self.zoomSpinBox, SIGNAL("valueChanged(int)"), self.showImage)
        editToolbar.addWidget(self.zoomSpinBox)
        
        self.addAction(self.imageLabel, (editUnMirrorAction))
        
        separator=QAction(self)
        sepqrator.setSeparator(True)
        self.addActions(editToolbar, (editUnMirrorAction, separator))
        
        self.resetableActions((editUnMirrorAction, True))
        
    def  addActions(self, target, actions):
        for action in actions:
            if action is None:
                target.addSeparator()
            else:
                target.addAction(action)

    def createAction(self, text, slot=None, shortcut=None, icon=None, tip=None, checkable=False, signal="triggered()"):
        action=QAction(text, self)
        if icon is not None:
            action.setIcon(text, self)
        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        if slot is not None:
            self.connect(action, SIGNAL(signal), slot)
        if checkable:
            action.setCheckable(True)
        return action
        
def main():
    app=AQpplication(sys.argv)
    app.setOrganizationName("Qtrac Ltd.")
    app.setOrganizationDomain("qtrac.eu")
    app.setApplicationName("Image Changer")
    app.setWindowIcon(QIcon(":/icon.png"))
    form=MainWindow()
    form.show()
    form.exec_()
    
        
