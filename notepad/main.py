#coding:utf-8

from handle import Handler
from random import choice
from config import connection
from time import time

URL_ENCODE='abbcdfghijklmnpoqrstuvwxyz123456'

def txt_by_url(url):
    url=url.lower()
    cursor=connection.cursor()
    cursor.execute('select txt from notepad where url=%s',url)
    txt=cursor.fetchone()
    if txt:
        txt=txt[0]
    else:
        txt=''
    return txt
class HandlerIndex(Handler):
    def get(self,url):
        if not url:
            while True:
                url=''.join(choice(URL_ENCODE)for i in xrange(9))
                if not txt_by_url(url):
                        break
            self.render(url)
        else:
            self.render('/index.html',txt=txt_by_url(url),url=url)
    def post(self,url):
        cursor=connection.cursor()
        url=url.lower()
        txt=self.get_argument('txt',False).rstrip()
        now=time()
        cursor.execute(
            'insert into notepad (url,txt,`time`) value '
            '(%s,%s,%s) ON DUPLICATE KEY UPDATE txt=%s,`time`=%s',
            (url,txt,now,txt,now)
            )
        self.finish({
            "time":now
        })
import tornado.web
from config import DEBUG
applecation=tornado.web.Application(
    [
        (r"/(.*)",HandlerIndex),
    ],
    debug=DEBUG
    )
if __name__=='__main__':
    import tornado.ioloop
    applecation.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

            



                        
                                
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                                
